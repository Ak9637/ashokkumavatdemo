package com.example.sahil.ashokkumavat.screens.details.di

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import com.example.sahil.ashokkumavat.base.MyViewModelFactory
import com.example.sahil.ashokkumavat.data.remote.RemoteRepository
import com.example.sahil.ashokkumavat.screens.details.MovieDetailsActivity
import com.example.sahil.ashokkumavat.screens.details.MovieDetailsContract
import com.example.sahil.ashokkumavat.screens.details.MovieDetailsViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class MovieDetailsModule {

    @Provides
    @Named(MovieDetailsActivity.TAG)
    fun provideMovieDetailsViewModel(activity: MovieDetailsActivity, @Named(MovieDetailsActivity.TAG) viewModelProvider: ViewModelProvider.Factory): MovieDetailsContract.ViewModel {
        return ViewModelProviders.of(activity,viewModelProvider).get(MovieDetailsViewModel::class.java)
    }

    @Provides
    @Named(MovieDetailsActivity.TAG)
    fun provideMovieDetailsViewModelFactory(remoteRepository: RemoteRepository): ViewModelProvider.Factory {
        return MyViewModelFactory(MovieDetailsViewModel(remoteRepository))
    }

}