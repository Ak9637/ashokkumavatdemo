package com.example.sahil.ashokkumavat.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class MyViewModelFactory<T:ViewModel>(model: T) : ViewModelProvider.Factory {
    private val viewModel: T =model

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return viewModel as T
    }
}