package com.example.sahil.ashokkumavat.screens.options.model

import android.os.Parcel
import android.os.Parcelable

data class OptionsList(val list: List<Option>) : Parcelable {


    constructor(source: Parcel) : this(
            source.createTypedArrayList(Option.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeTypedList(list)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<OptionsList> = object : Parcelable.Creator<OptionsList> {
            override fun createFromParcel(source: Parcel): OptionsList = OptionsList(source)
            override fun newArray(size: Int): Array<OptionsList?> = arrayOfNulls(size)
        }
    }
}