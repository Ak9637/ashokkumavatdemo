package com.example.sahil.ashokkumavat.data.remote

import com.example.sahil.ashokkumavat.data.remote.app.model.MoviesList
import retrofit2.Response
import retrofit2.http.*


interface ApiService {


    @GET("3/movie/popular")
    fun getMovies(@Query(ApiFeilds.API_KEY) apiKey: String,
                  @Query(ApiFeilds.PAGE) page: Int): io.reactivex.Observable<Response<MoviesList>>


}