package com.example.sahil.ashokkumavat.screens.details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.example.sahil.ashokkumavat.BuildConfig
import com.example.sahil.ashokkumavat.data.remote.RemoteRepository
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie
import com.example.sahil.ashokkumavat.screens.main.MainContract

class MovieDetailsViewModel(private val remoteRepo: RemoteRepository): ViewModel(), MovieDetailsContract.ViewModel{
    override fun getMovieDetails(id: Int): Movie? {
        return remoteRepo.getMovieDetails(id)
    }


}