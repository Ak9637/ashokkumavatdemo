package com.example.sahil.ashokkumavat.screens.main.di

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import com.example.sahil.ashokkumavat.base.MyViewModelFactory
import com.example.sahil.ashokkumavat.data.remote.RemoteRepository
import com.example.sahil.ashokkumavat.screens.main.MainActivity
import com.example.sahil.ashokkumavat.screens.main.MainContract
import com.example.sahil.ashokkumavat.screens.main.MainViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class MainModule {

    @Provides
    @Named(MainActivity.TAG)
    fun provideMainViewModel(activity: MainActivity, @Named(MainActivity.TAG) viewModelProvider: ViewModelProvider.Factory):MainContract.ViewModel {
        return ViewModelProviders.of(activity,viewModelProvider).get(MainViewModel::class.java)
    }

    @Provides
    @Named(MainActivity.TAG)
    fun provideMainViewModelFactory(remoteRepository: RemoteRepository): ViewModelProvider.Factory {
        return MyViewModelFactory(MainViewModel(remoteRepository))
    }

}