package com.example.sahil.ashokkumavat.di

import android.content.Context
import android.util.Log
import com.example.sahil.ashokkumavat.BuildConfig
import com.example.sahil.ashokkumavat.app.BaseApp
import com.example.sahil.ashokkumavat.di.MyAppScope
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named

/**
 * Created by ashok on 20/12/17.
 */

@Module
class NetworkModule {

    @Provides
    @MyAppScope
    fun getGson(): Gson {
        return Gson()
    }


    @Provides
    @MyAppScope
    fun httpLoggingInterceptor(): Interceptor {
        return HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            Log.i(BaseApp.APP_TAG, message)
        })
                .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @MyAppScope
    fun cache(cacheFile: File): Cache {
        return Cache(cacheFile, (20 * 1024 * 1024).toLong())  // 20 MB Cache File
    }

    @Provides
    @MyAppScope
    @Named("baseUrl")
    fun getBaseUrl(): String {
        return BuildConfig.SERVER_URL.trim()
    }


    @Provides
    @MyAppScope
    fun cacheFile(context: Context): File {
        return File(context.cacheDir, "okhttp-cache")
    }

    @Provides
    @MyAppScope
    fun okHttpClient(interceptor: Interceptor, cache: Cache): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout((BuildConfig.NETWORK_TIMEOUT).toLong(), TimeUnit.SECONDS)
                .readTimeout(BuildConfig.NETWORK_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .cache(cache)
                .build()
    }

}
