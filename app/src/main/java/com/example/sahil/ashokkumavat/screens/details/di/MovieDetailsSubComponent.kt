package com.example.sahil.ashokkumavat.screens.details.di

import com.example.sahil.ashokkumavat.di.MyActivityScope
import com.example.sahil.ashokkumavat.screens.details.MovieDetailsActivity
import com.example.sahil.ashokkumavat.screens.main.MainActivity
import com.example.sahil.ashokkumavat.screens.main.di.MainModule
import dagger.Subcomponent
import dagger.android.AndroidInjector

@MyActivityScope
@Subcomponent(modules = [(MovieDetailsModule::class)])
interface MovieDetailsSubComponent : AndroidInjector<MovieDetailsActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MovieDetailsActivity>()
}