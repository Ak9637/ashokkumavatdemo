package com.example.sahil.ashokkumavat.di

import android.app.Activity
import com.example.sahil.ashokkumavat.screens.details.MovieDetailsActivity
import com.example.sahil.ashokkumavat.screens.details.di.MovieDetailsSubComponent
import com.example.sahil.ashokkumavat.screens.main.MainActivity
import com.example.sahil.ashokkumavat.screens.main.di.MainSubComponent
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ActivityBuilders{


    @Binds
    @IntoMap
    @ActivityKey(MainActivity::class)
    abstract fun bindMainActivityInjectorFactory(builder: MainSubComponent.Builder): AndroidInjector.Factory<out Activity>


    @Binds
    @IntoMap
    @ActivityKey(MovieDetailsActivity::class)
    abstract fun bindMovieDetailsActivityInjectorFactory(builder: MovieDetailsSubComponent.Builder): AndroidInjector.Factory<out Activity>

}
