package com.example.sahil.ashokkumavat.di

import com.example.sahil.ashokkumavat.data.remote.ApiService
import com.example.sahil.ashokkumavat.data.remote.RemoteRepository
import com.example.sahil.ashokkumavat.data.remote.RemoteRepositoryImp
import com.example.sahil.ashokkumavat.di.MyAppScope
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module(includes = arrayOf(RetrofitModule::class))
class RepositoryModule{

    @Provides
    @MyAppScope
    fun getApiService(retrofit:Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @MyAppScope
    fun getApiRepository(apiService: ApiService): RemoteRepository {
        return RemoteRepositoryImp(apiService)
    }


}