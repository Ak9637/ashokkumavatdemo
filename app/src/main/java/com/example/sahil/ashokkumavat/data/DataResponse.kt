package com.example.sahil.ashokkumavat.data
import android.arch.lifecycle.MutableLiveData
import android.support.annotation.NonNull
import android.support.annotation.Nullable
import android.text.TextUtils
import com.example.sahil.ashokkumavat.app.Constants
import com.example.sahil.ashokkumavat.data.remote.ApiFeilds
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONObject


data class DataResponse<T>(@Nullable var statusCode:Int, @NonNull var status:String, @Nullable var payload:T, @Nullable val messages:List<String>?){
    companion object {
        const val SUCCESS="SUCCESS"
        const val FAILED="FAILED"
        const val ERROR="ERROR"

        fun <T> success(data: T?): DataResponse<T?> {
            return DataResponse<T?>(200, SUCCESS, data, null)
        }


        fun <T> error(errorCode:Int,msgs: List<String>): DataResponse<T?> {
            return DataResponse<T?>(errorCode, ERROR, null, msgs)
        }
        fun <T> error(errorCode:Int,msg: String?): DataResponse<T?> {

            val tempMssgs=ArrayList<String>()
            if(TextUtils.isEmpty(msg)){
                tempMssgs.add(Constants.SOMETHING_WENT_WRONG)
            }else{
                tempMssgs.add(msg!!)
            }

            return DataResponse<T?>(errorCode, ERROR, null, tempMssgs)
        }

        fun <T> error(errorBody: ResponseBody): DataResponse<T?> {

            val errorString = errorBody.string()
            val tempMssgs=ArrayList<String>()
            var errorCode=420

            if(errorString==null || errorString.isEmpty()){
                tempMssgs.add(Constants.SOMETHING_WENT_WRONG)
            }else{
                val errorJSON=JSONObject(errorString)
                if(errorJSON.length()>0){
                    if(errorJSON.has(ApiFeilds.MESSAGE)){
                        tempMssgs.add(errorJSON.getString(ApiFeilds.MESSAGE))
                    }else{
                        tempMssgs.add(Constants.SOMETHING_WENT_WRONG)
                    }

                    if(errorJSON.has(ApiFeilds.CODE)){
                        errorCode=errorJSON.getInt(ApiFeilds.CODE)
                    }

                }else{
                    tempMssgs.add(Constants.SOMETHING_WENT_WRONG)
                }
            }


            return DataResponse<T?>(errorCode, ERROR, null, tempMssgs)
        }



    }

    fun getErrorString():String{
        var error:String=""
        if(messages!=null && messages.isNotEmpty()){
            messages.map {
                error+=it+"\n"
            }
        }
        return error
    }

    fun isSuccess():Boolean{ return status==SUCCESS}
    fun isError():Boolean{ return status!=SUCCESS}


}