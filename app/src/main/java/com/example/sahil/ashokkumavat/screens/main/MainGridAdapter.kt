package com.example.sahil.ashokkumavat.screens.main

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import com.example.sahil.ashokkumavat.R
import com.example.sahil.ashokkumavat.screens.options.model.Option
import com.squareup.picasso.Picasso
import rx.subjects.PublishSubject
import java.util.*


class MainGridAdapter(private var items:ArrayList<Movie>):BaseAdapter(),Filterable{


    private var filterList = ArrayList<Movie>()
    val itemClickPublisher:PublishSubject<MovieGridItem> = PublishSubject.create()

    private var filterS=""
    private var sortByS="name"

    init {
        filterList.addAll(items)
    }
    override fun getFilter(): Filter {
        return filterObj
    }

    private val filterObj=object : Filter() {

        override fun performFiltering(charSequence: CharSequence?): FilterResults {
            filterS = charSequence?.toString() ?: ""
            val filterItems = ArrayList<Movie>()

            if (filterS.isEmpty()) {
                filterList.clear()
                filterList.addAll(items)
            } else {
                for (movie in items) {
                    if (movie.originalTitle!!.contains(filterS, true)) {
                        filterItems.add(movie)
                    }
                }

                filterList.clear()
                filterList.addAll(filterItems)
            }

            val filterResult = FilterResults()
            filterResult.values = filterList
            return filterResult
        }


        override fun publishResults(p0: CharSequence?, p1: FilterResults?) {

            this@MainGridAdapter.filterList=if(p1?.values== null){
               ArrayList<Movie>()
            }else{
                p1!!.values as ArrayList<Movie>
            }
            notifyDataSetChanged()
        }

    }



    override fun getItem(p0: Int): Movie {
        return filterList[p0]
    }

    override fun getItemId(p0: Int): Long {
        return 1
    }

    override fun getCount(): Int {
        return filterList.size


    }

    fun updateData(newItems:List<Movie?>){
        for(newItem in newItems){
            if(!items.contains(newItem)){
                items.add(newItem!!)
            }
        }
        filter.filter(filterS)
       // sortBy(Option(sortByS,"-1"))
    }

    fun sortBy(option: Option){
        sortByS=option.text
        when {
            sortByS=="name" -> {
                //sort by name
                filterList.sortWith(Comparator { m1, m2 -> if(m1==null || m2==null || m1.originalTitle==null || m2.originalTitle==null){
                    return@Comparator 0
                }else if(m1.originalTitle > m2.originalTitle){
                    return@Comparator 1
                }else{
                    return@Comparator -1
                }})
                notifyDataSetChanged()

            }
            sortByS.isEmpty() -> {
                //do nothing
            }
            else -> {
                //sort by votes
                filterList.sortWith(Comparator { m1, m2 -> if(m1==null || m2==null || m1.voteCount==null || m2.voteCount==null){
                    return@Comparator 0
                }else if(m1.voteCount > m2.voteCount){
                    return@Comparator 1
                }else{
                    return@Comparator -1
                }})
                notifyDataSetChanged()
            }
        }
    }

    override fun getView(p0: Int, convertView: View?, parent: ViewGroup): View {
        val view= LayoutInflater.from(parent.context).inflate(R.layout.layout_movie_item, null);
        val iv=view.findViewById<ImageView>(R.id.mainGridItemIv)

        Picasso.get()
                .load(iv.context.resources.getString(R.string.imgBasePath)+filterList[p0].posterPath)
                .placeholder(R.mipmap.ic_launcher)
                .into(iv)

        iv.setOnClickListener{
            itemClickPublisher.onNext(MovieGridItem(filterList[p0].id,iv))
        }


        return view
    }
    data class MovieGridItem(val id:Int,val view:View)

}