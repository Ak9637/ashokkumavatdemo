package com.example.sahil.ashokkumavat.di

import android.app.Application
import android.content.Context
import com.example.sahil.ashokkumavat.app.BaseApp
import com.example.sahil.ashokkumavat.di.MyAppScope
import com.example.sahil.ashokkumavat.screens.details.di.MovieDetailsSubComponent
import com.example.sahil.ashokkumavat.screens.main.di.MainSubComponent
import dagger.Module
import dagger.Provides

/**
 * Created by ashok on 20/12/17.
 */
@Module(subcomponents = arrayOf(
        MainSubComponent::class,
        MovieDetailsSubComponent::class
         ))


class AppModule {
    @Provides
    @MyAppScope
    internal fun provideContext(application: BaseApp): Context {
        return application.applicationContext
    }
    @Provides
    @MyAppScope
    internal fun provideApplication(application: BaseApp): Application {
        return application
    }
}