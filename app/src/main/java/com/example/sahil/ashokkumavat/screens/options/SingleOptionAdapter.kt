package com.example.sahil.ashokkumavat.screens.options

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.RadioButton
import com.example.sahil.ashokkumavat.R
import com.example.sahil.ashokkumavat.screens.options.model.Option
import com.example.sahil.ashokkumavat.screens.options.model.OptionsList


class SingleOptionAdapter(private val optionsList: OptionsList, var selectedItemId:String) : RecyclerView.Adapter<SingleOptionAdapter.MyViewHolder>() {

    var listener: SingleOptionsDialog.OnSelectedOptionChangedListener? = null

    fun setOnSelectedOptionChangedListener(newListener: SingleOptionsDialog.OnSelectedOptionChangedListener) {
        this.listener = newListener
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var radioButton:RadioButton = view.findViewById(R.id.choiceOptionRadioBt)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.choice_option_row, parent, false)
        return MyViewHolder(itemView)
    }


    override fun getItemCount(): Int {
        return optionsList.list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = optionsList.list[position]
        holder.radioButton.text = item.text
        holder.radioButton.isChecked = item.id == selectedItemId
        holder.radioButton.setOnCheckedChangeListener { _, isChecked ->
            run{
                if(selectedItemId!=item.id && isChecked){
                    selectedItemId=item.id
                    holder.radioButton.post {
                        notifyDataSetChanged()
                    }
                    listener?.onSelectedOptionChanged(optionsList.list[position])
                }
            }
        }
    }

    fun getSelectedItem(): Option? {
        var selectedOption : Option?=null
        for(option in optionsList.list){
            if(option.id==selectedItemId){
                selectedOption=option
                break
            }
        }
        return selectedOption
    }



}