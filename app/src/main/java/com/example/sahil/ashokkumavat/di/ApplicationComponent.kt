package com.example.sahil.ashokkumavat.di


import com.example.sahil.ashokkumavat.app.BaseApp
import com.example.sahil.ashokkumavat.di.ActivityBuilders
import com.example.sahil.ashokkumavat.di.MyAppScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by ashok on 20/12/17.
 */
@MyAppScope
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityBuilders::class,
        RepositoryModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun injectApplication(application: BaseApp): Builder
        fun build(): ApplicationComponent
    }

    abstract fun inject(application: BaseApp)


}
