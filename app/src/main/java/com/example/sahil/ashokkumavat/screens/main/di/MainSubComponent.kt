package com.example.sahil.ashokkumavat.screens.main.di

import com.example.sahil.ashokkumavat.di.MyActivityScope
import com.example.sahil.ashokkumavat.screens.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@MyActivityScope
@Subcomponent(modules = [(MainModule::class)])
interface MainSubComponent : AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}