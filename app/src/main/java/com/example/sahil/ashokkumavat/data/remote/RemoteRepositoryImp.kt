package com.example.sahil.ashokkumavat.data.remote

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.sahil.ashokkumavat.data.DataResponse
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie
import com.example.sahil.ashokkumavat.data.remote.app.model.MoviesList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response


class RemoteRepositoryImp(private val apiService: ApiService) : RemoteRepository {


    val data = MutableLiveData<MoviesList>()
    val moviesList = MoviesList(0,100000,ArrayList(),1)


    override fun getMovies(apiKey: String, pageNo: Int): LiveData<MoviesList> {
        if (moviesList.page < pageNo && pageNo<moviesList.totalPages) {
            // get new movies from remote
            apiService.getMovies(apiKey, pageNo)
                    .retry(3)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableObserver<Response<MoviesList>>(){
                        override fun onComplete() {
                        }

                        override fun onNext(response: Response<MoviesList>) {
                            if(response.code()==200 && response.body()!=null){
                                val data=response.body()!!.results
                                moviesList.page=response.body()!!.page
                                moviesList.totalPages=response.body()!!.totalPages
                                for(movie in data){
                                    if(!moviesList.results.contains(movie)){
                                        moviesList.results.add(movie)
                                    }
                                }
                            }
                            data.postValue(moviesList)
                        }

                        override fun onError(e: Throwable) {

                        }

                    })

        } else {
            // provide existing data
            data.postValue(moviesList)
        }
        return data
    }

    override fun getMovieDetails(id: Int): Movie? {
        if(moviesList.results.isNotEmpty()){
            for(movie in moviesList.results){
                if(movie?.id==id){
                    return movie
                }
            }
        }
        return null
    }


}













