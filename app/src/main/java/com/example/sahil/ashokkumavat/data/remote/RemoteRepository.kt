package com.example.sahil.ashokkumavat.data.remote

import android.arch.lifecycle.LiveData
import com.example.sahil.ashokkumavat.data.DataResponse
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie
import com.example.sahil.ashokkumavat.data.remote.app.model.MoviesList

interface RemoteRepository {
    fun getMovies(apiKey:String,pageNo:Int): LiveData<MoviesList>
    fun getMovieDetails(id:Int):Movie?
}

