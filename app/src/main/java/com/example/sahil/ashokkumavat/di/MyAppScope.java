package com.example.sahil.ashokkumavat.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by osp158 on 10/5/17.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface MyAppScope {
}
