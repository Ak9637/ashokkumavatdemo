package com.example.sahil.ashokkumavat.data.remote.app.model

import com.google.gson.annotations.SerializedName

data class MoviesList(
		@field:SerializedName("page")
		var page: Int,

		@field:SerializedName("total_pages")
		var totalPages: Int,

		@field:SerializedName("results")
		var results: ArrayList<Movie?>,

		@field:SerializedName("total_results")
		var totalResults: Int
)
