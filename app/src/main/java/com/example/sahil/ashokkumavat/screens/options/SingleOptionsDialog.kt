package com.example.sahil.ashokkumavat.screens.options

import android.content.DialogInterface
import android.content.res.ColorStateList
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import kotlinx.android.synthetic.main.dialog_options.*
import rx.Single
import android.widget.RadioButton
import android.widget.RadioGroup
import com.example.sahil.ashokkumavat.R
import com.example.sahil.ashokkumavat.screens.options.model.Option
import com.example.sahil.ashokkumavat.screens.options.model.OptionsList
import org.jetbrains.anko.button
import org.jetbrains.anko.support.v4.toast


class SingleOptionsDialog : DialogFragment() {

    var optionsList: OptionsList?=null
    var selectedId: String = "-1"
    var title = "Select"
    var listener: OnSelectedOptionChangedListener? = null
    lateinit var adapter: SingleOptionAdapter

    companion object {
        val OPTIONS_LIST = "optionsList"
        val SELECTED_ID = "selectedId"
        val TITLE = "title"
        fun getNewDialog(fragmentManager: FragmentManager,tag:String,list: OptionsList, title: String, selectedId: Int): SingleOptionsDialog  {
            val dialog = SingleOptionsDialog()
            val bundle = Bundle()
            bundle.putParcelable(OPTIONS_LIST, list)
            bundle.putInt(SELECTED_ID, selectedId)
            bundle.putString(TITLE, title)
            dialog.arguments = bundle
            return dialog
        }
        fun getNewDialog(fragmentManager: FragmentManager,tag:String,list: OptionsList, title: String, selectedId: String?): SingleOptionsDialog  {
            val dialog = SingleOptionsDialog()
            val bundle = Bundle()
            bundle.putParcelable(OPTIONS_LIST, list)
            if(selectedId!=null && selectedId.isNotEmpty()){
                bundle.putString(SELECTED_ID, selectedId)
            }else{
                bundle.putString(SELECTED_ID,"-1")
            }
            bundle.putString(TITLE, title)
            dialog.arguments = bundle
            return dialog
        }
        fun showNewDialog(fragmentManager: FragmentManager,tag:String,list: OptionsList, title: String, selectedId: Int): SingleOptionsDialog  {
            val dialog = SingleOptionsDialog()
            val bundle = Bundle()
            bundle.putParcelable(OPTIONS_LIST, list)
            bundle.putInt(SELECTED_ID, selectedId)
            bundle.putString(TITLE, title)
            dialog.arguments = bundle
            dialog.show(fragmentManager,tag)
            return dialog
        }
        fun showNewDialog(fragmentManager: FragmentManager,tag:String,list: OptionsList, title: String, selectedId:String?): SingleOptionsDialog  {
            val dialog = SingleOptionsDialog()
            val bundle = Bundle()
            bundle.putParcelable(OPTIONS_LIST, list)
            if(selectedId!=null && selectedId.isNotEmpty()){
                bundle.putString(SELECTED_ID, selectedId)
            }else{
                bundle.putString(SELECTED_ID, "-1")
            }
            bundle.putString(TITLE, title)
            dialog.arguments = bundle
            dialog.show(fragmentManager,tag)
            return dialog
        }


    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments?.getParcelable<OptionsList>(OPTIONS_LIST) != null) {
            optionsList = arguments!!.getParcelable(OPTIONS_LIST)
            selectedId = arguments!!.getString(SELECTED_ID)
            title = arguments!!.getString(TITLE)
        }
        //adapter = SingleOptionAdapter(optionsList!!, selectedId)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.dialog_options, container,
                false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun init() {
        dialogOptionsTitleTv.text = title
        //dialogOptionsRv.applyVerticalLinearLayoutManager()
        //dialogOptionsRv.adapter = adapter

        for (i in optionsList!!.list) {
            val button = RadioButton(activity)
            button.text = "${i.text}"
            button.setPrimaryColorButtonTint()
            button.minHeight=resources.getInteger(R.integer.minRadioButtonHeight)
            button.isChecked = selectedId==i.id
            dialogOptionsRadioGroup.addView(button)
        }


        dialogOptionsRadioGroup.setOnCheckedChangeListener { _, checkedItemId -> run{
            //toast(""+checkedItemId)
            val radioButtonID = dialogOptionsRadioGroup.checkedRadioButtonId
            val radioButton:RadioButton = dialogOptionsRadioGroup.findViewById(radioButtonID) as RadioButton
            val idx = dialogOptionsRadioGroup.indexOfChild(radioButton)

            val itemSelected=optionsList!!.list[idx]
            selectedId=itemSelected.id
            listener?.onSelectedOptionChanged(itemSelected)
            dismiss()
        }}


    }

    interface OnSelectedOptionChangedListener {
        fun onSelectedOptionChanged(option: Option)
    }

    fun setOnSelectedOptionChangedListener(newListener: OnSelectedOptionChangedListener) {
        this.listener = newListener
    }

    fun sendCurrentSelectedOption(){
        try {
            var selectedOption: Option? = adapter.getSelectedItem()
            if (selectedOption != null) {
                listener?.onSelectedOptionChanged(selectedOption)
            }
        } catch (e: Exception) {
        }

    }

    fun getOptionById(id:String):Option?{
        if(optionsList!=null && optionsList!!.list.isNotEmpty()){
            for(option in optionsList!!.list){
                if(option.id==id){
                    return option
                }
            }
        }

        return null
    }

    override fun onStop() {
        sendCurrentSelectedOption()
        super.onStop()

    }

    fun RadioButton.setPrimaryColorButtonTint() {
        //if (Build.VERSION.SDK_INT >= 21) {

        val colorStateList = ColorStateList(
                arrayOf(

                        intArrayOf(-android.R.attr.state_enabled), //disabled
                        intArrayOf(android.R.attr.state_enabled) //enabled
                ),
                intArrayOf(

                        resources.getColor(R.color.material_grey_800), //disabled
                        resources.getColor(R.color.cerulean) //enabled
                )
        )


        this.buttonTintList = colorStateList//set the color tint list
        this.invalidate() //could not be necessary

    }





}

