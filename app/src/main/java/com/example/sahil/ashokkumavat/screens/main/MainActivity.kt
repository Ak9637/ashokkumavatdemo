package com.example.sahil.ashokkumavat.screens.main

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AbsListView
import android.widget.AdapterView
import com.example.sahil.ashokkumavat.R
import com.example.sahil.ashokkumavat.screens.details.MovieDetailsActivity
import com.example.sahil.ashokkumavat.screens.options.SingleOptionsDialog
import com.example.sahil.ashokkumavat.screens.options.model.Option
import com.example.sahil.ashokkumavat.screens.options.model.OptionsList
import com.jakewharton.rxbinding.widget.RxTextView
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import rx.observers.Observers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

class MainActivity: AppCompatActivity() {

    companion object {
        const val TAG="MAIN_SCREEN"
    }

    @Inject
    @field: Named(TAG)
    lateinit var viewModel: MainContract.ViewModel

    private val mAdapter=MainGridAdapter(ArrayList())

    private var sortDialog:SingleOptionsDialog?=null
    private lateinit var sortOptionsList:ArrayList<Option>
    private var selectedSortOption:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        inits()
        loadData()
    }

    private fun loadData() {
        showLoading()
        viewModel.getNextPage().observe(this, Observer {
            if(it==null){
                mAdapter.updateData(ArrayList())
            }else{
                mAdapter.updateData(it)
            }
            mAdapter.notifyDataSetChanged()
            dismissLoading()
        })
    }

    private fun showLoading()
    {
        mainProgressBar.visibility= View.VISIBLE

    }
    private fun dismissLoading()
    {
        mainProgressBar.visibility= View.GONE

    }



    private fun inits() {

        sortOptionsList=ArrayList<Option>()

        sortOptionsList.add(Option("name","1"))
        sortOptionsList.add(Option("votes","2"))



        mainGridView.adapter=mAdapter
        mAdapter.itemClickPublisher.subscribe {
            MovieDetailsActivity.start(this,it.id,it.view)
        }
        mainGridView.setOnScrollListener(object:AbsListView.OnScrollListener{
            override fun onScroll(p0: AbsListView?, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                if (totalItemCount > 0) {
                    val lastVisibleItem = firstVisibleItem + visibleItemCount
                    if(totalItemCount-lastVisibleItem<2){
                        loadData()
                    }
                }
            }

            override fun onScrollStateChanged(p0: AbsListView?, p1: Int) {
            }
        })

        mainSearchEt.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                mAdapter.filter.filter(p0)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })


        mainSortIvBt.setOnClickListener{
            if(sortDialog==null || !sortDialog!!.isVisible){
                sortDialog= SingleOptionsDialog.Companion.getNewDialog(this.supportFragmentManager,"sortOptions",OptionsList(sortOptionsList),"Sort By",selectedSortOption)
                sortDialog?.listener=object :SingleOptionsDialog.OnSelectedOptionChangedListener{
                    override fun onSelectedOptionChanged(option: Option) {
                        mAdapter.sortBy(option)
                        selectedSortOption=option.text
                    }
                }
                sortDialog?.show(this.supportFragmentManager,"sortDialog")
            }
        }


    }

}