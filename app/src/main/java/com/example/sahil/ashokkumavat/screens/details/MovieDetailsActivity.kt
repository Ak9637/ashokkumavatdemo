package com.example.sahil.ashokkumavat.screens.details

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.sahil.ashokkumavat.R
import com.example.sahil.ashokkumavat.screens.main.MainActivity
import com.example.sahil.ashokkumavat.screens.main.MainContract
import com.example.sahil.ashokkumavat.screens.main.MainGridAdapter
import com.squareup.picasso.Picasso
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_movie_details.*
import javax.inject.Inject
import javax.inject.Named
import android.support.v4.app.ActivityOptionsCompat
import android.view.View


class MovieDetailsActivity : AppCompatActivity() {

    companion object {
        const val TAG="MovieDetails"
        fun start(activity:Activity,id:Int,transitionFrom: View){
            val intent= Intent(activity,MovieDetailsActivity::class.java)
            intent.putExtra("id",id)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionFrom, "detailsIv")
            activity.startActivity(intent,options.toBundle())
        }
    }

    @Inject
    @field: Named(TAG)
    lateinit var viewModel: MovieDetailsContract.ViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        inits()
        loadMovieDetailsFor(intent.extras.getInt("id"))
    }

    private fun loadMovieDetailsFor(id: Int) {
        val movie= viewModel.getMovieDetails(id)
        if(movie==null){
            // if movie details not found,exit
            onBackPressed()
        }else{
            Picasso.get()
                    .load(resources.getString(R.string.imgBasePath)+movie!!.posterPath)
                    .into(movieDetailsPosterIv)

            movieDetailsTitleTv.text= "Title :\n${movie.originalTitle}"
            movieDetailsDescTv.text= "Overview:\n${movie.overview}"
            movieDetailsVotesTv.text= "Votes: ${movie.voteCount.toString()}"
            movieDetailsReleaseDateTv.text= "Released on: ${movie.releaseDate}"
        }

    }

    override fun onBackPressed() {
        finish()
        //super.onBackPressed()

    }

    private fun inits() {
    }
}
