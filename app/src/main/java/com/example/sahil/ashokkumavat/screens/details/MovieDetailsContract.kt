package com.example.sahil.ashokkumavat.screens.details

import android.arch.lifecycle.LiveData
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie

class MovieDetailsContract{

    interface ViewModel{
        fun getMovieDetails(id:Int): Movie?
    }

}