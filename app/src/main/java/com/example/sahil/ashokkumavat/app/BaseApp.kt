package com.example.sahil.ashokkumavat.app

import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks2
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import android.content.Intent
import com.example.sahil.ashokkumavat.di.DaggerApplicationComponent
import okhttp3.OkHttpClient


class BaseApp: Application(),HasActivityInjector{
    companion object {
        const val APP_TAG = "Ak"
        @Volatile
        lateinit var mInstance: Application
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {

        mInstance = this
        DaggerApplicationComponent
                .builder()
                .injectApplication(this)
                .build()
                .inject(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        init()

        super.onCreate()


    }
    private fun init() {

        try{


        }catch (e:Exception){}
    }
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.d(APP_TAG, level.toString() + "")
        System.gc()
        io.reactivex.Observable.just(level)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .doOnError { e -> e.printStackTrace() }
                .subscribe { level ->
                    run {
                        if (level <= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW) {
                           // Fresco.getImagePipeline().clearCaches()
                           // Fresco.getImagePipeline().clearDiskCaches()
                        }

                    }
                }

    }

    private val unCaughtExceptionHandler = Thread.UncaughtExceptionHandler { _, e -> e.printStackTrace() }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }



}