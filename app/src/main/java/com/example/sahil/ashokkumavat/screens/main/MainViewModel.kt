package com.example.sahil.ashokkumavat.screens.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.example.sahil.ashokkumavat.BuildConfig
import com.example.sahil.ashokkumavat.data.remote.RemoteRepository
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie

class MainViewModel(private val remoteRepo: RemoteRepository):ViewModel(),MainContract.ViewModel{
    var currentPage=0


    override fun getNextPage(): LiveData<ArrayList<Movie?>> {
        currentPage++
        return Transformations.map(remoteRepo.getMovies(BuildConfig.API_KEY,currentPage)) {
            return@map it.results
        }
    }

}