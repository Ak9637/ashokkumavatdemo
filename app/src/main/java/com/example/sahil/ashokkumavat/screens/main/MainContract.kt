package com.example.sahil.ashokkumavat.screens.main

import android.arch.lifecycle.LiveData
import com.example.sahil.ashokkumavat.data.remote.app.model.Movie

class MainContract{

    interface ViewModel{
        fun getNextPage():LiveData<ArrayList<Movie?>>
    }

}