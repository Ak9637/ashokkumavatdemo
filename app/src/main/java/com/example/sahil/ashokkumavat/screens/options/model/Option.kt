package com.example.sahil.ashokkumavat.screens.options.model

import android.os.Parcel
import android.os.Parcelable

data class Option(var text: String, val id: String) : Parcelable {

    var data:String?=null

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
        data = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(text)
        parcel.writeString(id)
        parcel.writeString(data)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Option> {
        override fun createFromParcel(parcel: Parcel): Option {
            return Option(parcel)
        }

        override fun newArray(size: Int): Array<Option?> {
            return arrayOfNulls(size)
        }
    }


}